# Formulärdemo

## Datastruktur

Här har jag skapat två datastrukturer. Den ena är en (mycket förenklad) ansökan:

```
titel: Konsekvenser av att återutsätta fisk
beskrivning: Lorem ipsum quod odit ...
projekttid: 3 # år
startdatum: '2023-01-01'
sökt-belopp: 80000000
doktorsexamen: True

```

Den andra strukturen är en beskrivning av den första strukturen, ansökansstrukturen. Den beskriver vilka fält som ska finnas med, vilken datatyp varje fält ska ha, om det finns något min- och maxvärde eller något mönster som svaret behöver följa.

```
# Schema
type: map
mapping:
  titel:
    type: str
    required: True
  beskrivning:
    type: str
    required: True
    range:
      min: 100
...
```

## Validera

Med hjälp av den senare strukturen kan man validera ansökansstrukturen. Här använder vi verktyget [pykwalify](). För att installera det, skriv `pip3 install pikwlify`. För att validera, skriv

`pykwalify --schema-file utlysning1-schema.yaml --data-file ansokan1.yaml`

## Rendera

Använd skriptet 'build-form.py' för att bygga ett formulär utifrån skriptet. Skriptet tar en parameter: schemafilen, exempel:

`python3 build-form.py utlysning1-schema.yaml`

## Slutsats

Schemastrukturen kan användas för att beskriva en (grovt förenklad) ansökan, validera en sådan ansökansfil samt rendera ett formulär utifrån schemat.

### Nästa steg

Nästa steg blir att bygga funktionalitet för att få formuläret att kunna skapa en ansökansfil utifrån ifyllda uppgifter.
