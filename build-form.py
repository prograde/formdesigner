#!/usr/bin/env python3

import sys
import yaml

def get_schema(filename):
    with open(filename) as file:
        return yaml.safe_load(file)

def stringbox(key, value):
    return f"<li><label>{key}</label><input type=\"text\" /></li>\n"

def intbox(key, value):
    return f"<li><label>{key}</label><input type=\"text\" /></li>\n"

def datebox(key, value):
    return f"<li><label>{key}</label><input type=\"text\" /></li>\n"

def checkbox(key, value):
    return f"<li class=\"checkbox\"><label>{key}</label><input type=\"checkbox\" /></li>\n"

def dropdown(key, value):
    options = value['enum']
    control = f"<li>\n<label>{key}</label>\n"
    control += f"<select>\n  <option value=\"\">Välj {key}</option>\n  "
    for opt in options:
        control += f"<option value=\"{opt}\">{opt}</option>\n  "
    control = control[0:-2] + "</select>\n</li>\n"
    return control

def build_control(key, value):
    controltype = value['type']
    if 'enum' in value.keys():
        controltype = (controltype, 'enum')

    controlbuilders = {
            ('str', 'enum'): dropdown,
            'str': stringbox,
            'int': intbox,
            'date': datebox,
            'bool': checkbox
            }
    return controlbuilders.get(controltype)(key, value)

def build_form(schema):
    document = "<ul>\n"
    if schema['type'] == 'map':
        for key, value in schema['mapping'].items():
            document += build_control(key, value)
    return document + "</ul>"

def put_form_in_template(form):
    template_filename = 'html-template.html'
    template = open(template_filename).read()
    formpage = template.replace("{{form}}", form)
    return formpage

if __name__ == '__main__':
    if not sys.argv[1]:
        exit -1
    filename = sys.argv[1]
    schema = get_schema(filename)
    form = build_form(schema)
    # form = put_form_in_template(form)
    print(form)
